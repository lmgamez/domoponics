# Admin needs to run an app on dokku



## Dokku installation

Set hostname in the VM:
```sh
hostnamectl set-hostname <host_name>
hostnamectl
```

```sh
wget -NP . https://dokku.com/install/v0.30.6/bootstrap.sh
sudo DOKKU_TAG=v0.30.6 bash bootstrap.sh
```

Add client ssh key in dokku
```sh
cat <path/to/client_key.pub> | dokku ssh-keys:add <client_user_name>
```

Set domain:
```sh
dokku domains:set-global <domain_name>
```
This might need a bit of thinking. Nginx subdomain might not work if given a plain IP address, but we can't rely on sslio.io service to get domain names.

Install pluggins:
```sh
sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git
sudo dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git
dokku letsencrypt:set --global email <client_email>
dokku letsencrypt:cron-job --add
```

## Create the app (TODO: Should provide a script for this part)

#### Initialize app:
```sh
dokku apps:create <app_name>
```

#### Create and ling database service
Here with postgres:
```sh
dokku postgres:create <database_name>
dokku postgres:link <database_name> <app_name>
```

#### Add domains to app

```sh
dokku domains:add <app_name> <domain>
```
most likely `dokku domains:add <app_name> <app_name>`

!!! info inline end "TODO:"
    Find how to auto domain a new app

#### TLS cert

```sh
dokku letsencrypt:enable <app_name>
```

## Runner for CI/CD

To allow user to deploy the app by pushing to gitlab, we need a runner somewhere (location to discuss, either a dedicated VM or each dokky server also being their own runner).


#### Install Docker
From https://docs.docker.com/engine/install/ubuntu/:

Update the `apt` package index and install packages to allow `apt` to use a repository over HTTPS:
```sh
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
```

Add Docker’s official GPG key:
```sh
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```

Use the following command to set up the repository:
```sh
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Update the `apt` package index:
```sh
sudo apt-get update
```

Install Docker Engine, containerd, and Docker Compose.
```sh
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

(opt)Verify that the Docker Engine installation is successful by running the hello-world image.
```sh
sudo docker run hello-world
```

#### Install gitlab runner

Download the binary for your system
```sh
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
```

Give it permission to execute
```sh
sudo chmod +x /usr/local/bin/gitlab-runner
```
Create a GitLab Runner user
```sh
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```

Install and run as a service
```sh
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

#### Add an app repo to the runner
You need to ask the client (or they will contact you to set up this) for their project `URL` and `Registration token`. Then run:
```sh
gitlab-runner register --url <client_url> --registration-token <client_token>
```
The prompt will ask you more information:
-URL: Leave blank as we set up the default URL
-Token: Leave blank aswell
-Description: Add correct description for the runner
-tags: dokku
-Maintenance note: free field
-