# User needs to deploydjango app to Dokku

#### Requirements.txt

Add in requirements.txt all the deployment necessary packages:
```sh sh title="requirements.txt"
django>=4.0,<5.0
psycopg2==2.9.3
dj-database-url==1.0.0
gunicorn==20.1.0
#plus project python dependencie
```
#### Using the correct database
Make sure that your app is using the correct database. The database is given by dokku using the environment variable `DATABASE_URL`
##### DJANGO example: Modifying settings.py for deployment
Add the correct dataset. Link to dataset is provided to the app via env var `DATABASE_URL`
```py
if "DATABASE_URL" in os.environ:
    DATABASES = {
        'default': dj_database_url.config(conn_max_age=MAX_CONN_AGE, ssl_require=False)
    }
```
Add secret key in env (not sure)

#### Add procfile:
Standard deployment for django with gunicorn looks like this:
```sh
web: gunicorn core.wsgi:application

release: export DJANGO_SETTINGS_MODULE=<pythonpath to setting> ; django-admin migrate --no-input && django-admin collectstatic --no-input
```

#### Runtime.txt
Adding python version needed to run django
```sh title="runtime.txt"
python-X.XX.X
```

#### Add dokku server in git
```sh
git remote add dokku dokku@<server_addr>:<app_name>
```

#### Push
!!! warning inline end  
    Main branch in gitlab must not be protected otherwise additionnal steps are needed (steps to be determined)
```sh
git push dokku main:master
```

## CI/CD with GitLab
It is possible to implement a CI/CD pipeline in your GitLab repository. For this you need:
- Register your project for a runner. On the GitLab repository webpage, `Settings > CI/CD`, expand `Runners`. Under `Project runners`, you will find two piece of information for the registration: `URL` and `registration token`.  You need to communicate this two value to the admin in charge of your dokky server.

- Create a .gitlab-ci.yml to specify a deploy method. Following is a solution that you can add to your repository (consult https://github.com/dokku/gitlab-ci for more information)

```sh title=".gitlab-ci.yml"
---
# Dokku deploy
image: dokku/ci-docker-image

stages:
  - deploy

variables:
  GIT_DEPTH: 0

deploy_dokku:
  stage: 
    - deploy
  only:
    - main
    - master
  environment:
    name: dokku
    url: https://domoponics.129.97.152.195.sslip.io/sensors/
  variables:
    GIT_REMOTE_URL: ssh://dokku@129.97.152.195:22/domoponics
  script: 
     - echo "Test deploy"
     - echo "test file" > ttt.txt
     - dokku-deploy
  after_script: 
     - dokku-unlock

```

#### New deploy method
Your app is automatically deploy whenever you push to the main branch:
```sh
git push
#OR
git push origin master
```

#### Gitlab email
You might notice that once the CI/CD is running, you will receive a notification email everytime you push to your main branch. To stop reveiving this email, go to your gitlab profile `Preferences > Notifications`, under `Projects` you should see your repository. You can create a custom notification profile: Deactivate `Fixed pipeline`.